<?php
session_start(); 
include_once 'php/dbconfig.php';
 
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>File Uploading With PHP and MySql</title>
</head>
<body>
<div class="container">

	<form action="php/upload.php" method="post" enctype="multipart/form-data" >
	<input type="file" name="file" />
	<button type="submit" name="btn-upload">upload</button>
	</form>
	
    <br /><br />
    <?php
	if(isset($_GET['success']))
	{
		?>
        <label>File Uploaded Successfully...  <a href="view.php">click here to view file.</a></label>
        <?php
	}
	else if(isset($_GET['fail']))
	{
		?>
        <label>Problem While File Uploading !</label>
        <?php
	}
	else
	{
		?>
        
        <?php
	}
	?>
</div>

</body>
</html>