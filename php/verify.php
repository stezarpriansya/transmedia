﻿<?php
  session_start();

  require_once 'config.php';

  /**
   * Include ircmaxell's password_compat library.
   */
  require '../lib/password.php';

  if(isset($_POST['submit'])){
    //Retrieve the field values from our login form.
    $nip = !empty($_POST['nip']) ? trim($_POST['nip']) : null;
    $passwordAttempt = !empty($_POST['pass']) ? trim($_POST['pass']) : null;

    try {
      $sql = "SELECT id, nip, password FROM user  WHERE nip = :nip";
      $stmt = $pdo->prepare($sql);

      //Bind value.
      $stmt->bindValue(':nip', $nip);

      //Execute.
      $stmt->execute();

      //Fetch row.
      $user = $stmt->fetch(PDO::FETCH_ASSOC);

      //If $row is FALSE.
      if($user === false){
          //Could not find a user with that nip!
          //PS: You might want to handle this error in a more user-friendly manner!
          echo '<script>alert("Incorrect nip!");</script>';
          header("Refresh: 0; URL = ../formlogin.php");
          die();
      } else{
          //User account found. Check to see if the given password matches the
          //password hash that we stored in our users table.

          //Compare the passwords.
          $validPassword = password_verify($passwordAttempt, $user['password']);

          //If $validPassword is TRUE, the login has been successful.
          if($validPassword){
              //Provide the user with a login session.
              //$row                  = $sql->fetch($sql);
              session_start();
              $_SESSION['nip'] = $user['nip'];
              $_SESSION['logged']   = TRUE;
              header("Location: ../index.php"); // Success!
              exit;

          } else{
              //$validPassword was FALSE. Passwords do not match.
              echo '<script>alert("Incorrect password!");</script>';
              header("Refresh: 0; URL = ../formlogin.php");
              die();
          }
      }
    }
    catch(PDOException $e)
    {
      die( print_r( $e->getMessage() ) );
    }

  }else{
      header("Location: ../index.php");
      exit;
}